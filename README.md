![Logo.png](https://bitbucket.org/repo/gkbbXyx/images/2915631825-Logo.png)

### Instalación ###
1.- Configurar **.ENV** de Laravel con la base de datos a utilizar ( copiar el ejemplo y modificarlo a su necesidad)

2.- Una vez configurada y creada la base de datos utilizaremos el comando 

```
#!php

php artisan migrate
```
*Si queremos cargar los datos de prueba agregamos **--seed** al comando de arriba*
```
#!php

php artisan migrate --seed
```

3.- Utilizar el siguiente comando para generar los tokens de passport
```
#!php
php artisan passport:install
```
4.- Copiamos el *"Cliente Secret"* del segundo cliente

![passport_install.png](https://bitbucket.org/repo/gkbbXyx/images/2624196339-passport_install.png)

5.- Abrimos el archivo encargado de hacer el login (resources/assets/js/components/authentication/mod/login.vue)
y pegamos el valor en el client_secret

6.- Compilamos los estilos y scripts usando
```
#!javascript

npm run production
```

7.- Una vez realizados estos pasos usaremos el comando
```
#!php
php artisan serve
```

Si usamos los *seeders* al correr el proyecto y accesar a http://localhost:8000 deberiamos ver la pagina principal con datos de prueba