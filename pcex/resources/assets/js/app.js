import Vue from 'vue'
import Router from './routes'
import App from './components/App.vue'
import Velocity from 'velocity-animate'

import VueResource from 'vue-resource'
Vue.use(VueResource)

import VueCarousel from 'vue-carousel';
Vue.use(VueCarousel)

import Toast from 'vue-easy-toast'
Vue.use(Toast)

import VuePaginate from 'vue-paginate'
Vue.use(VuePaginate)

import VueSweetAlert from 'vue-sweetalert'
Vue.use(VueSweetAlert)

import VeeValidate, {Validator} from 'vee-validate';
import es from 'vee-validate/dist/locale/es'
Validator.addLocale(es);
Vue.use(VeeValidate, {
    locale: 'es'
});

import axios from 'axios'

import {store} from './store.js'

import Auth from './packages/auth/Auth.js'
Vue.use(Auth)

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

axios.defaults.baseURL = 'http://localhost:8000'
axios.defaults.headers['Authorization'] = 'Bearer ' + Vue.auth.getToken()


// Vue.http.options.root = 'http://localhost:8000'
// Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken()

Router.beforeEach(
    (to, from, next) => {
        if (to.matched.some(record => record.meta.forVisitors)) {
            if (Vue.auth.isAuthenticated()) {
                next({
                    path: '/'
                })
            } else next()
        }
        else if (to.matched.some(record => record.meta.forAuth)) {
            if (!Vue.auth.isAuthenticated()) {
                next({
                    path: '/login'
                })
            } else next()
        }
        else if (to.matched.some(record => record.meta.forAdmin)) {
            if (!Vue.auth.isAuthenticated()) {
                next({
                    path: '/login'
                })
            } else {
                if (!store.getters.isAdmin) {
                    next({
                        path: '/'
                    })
                } else next()
            }
        }
        else next()
    }
)

const app = new Vue({
    el: '#app',
    render: h => h(App),
    router: Router,
    store,
    mounted() {
        if (this.$auth.isAuthenticated()) {
            if (!this.$store.getters.isAdmin) {
                let config = {
                    headers: {
                        Authorization: 'Bearer ' + Vue.auth.getToken()
                    }
                }

                axios.get('api/isAdmin', config)
                    .then(response => {
                        let isAdmin = response.data.isAdmin;
                        if (isAdmin) {
                            this.$store.dispatch('isAdmin', isAdmin)
                        }
                    })
            }
        }

        this.$store.dispatch('isAuthenticated', this.$auth.isAuthenticated())
        this.$store.dispatch('authenticatedUser', this.$auth.getAuthenticatedUser())
    }
})

