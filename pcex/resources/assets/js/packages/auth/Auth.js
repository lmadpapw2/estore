import {store} from '../../store'
export default function (Vue) {

    Vue.auth = {

        setToken (token,expiration) {
            localStorage.setItem('token',token)
            localStorage.setItem('expiration',expiration)
        },

        getToken (){
            var token = localStorage.getItem('token')
            var expiration = localStorage.getItem('expiration')
            let date = Math.floor(Date.now() / 1000);

            if(!token || !expiration)
                return null

            if(date > parseInt(expiration)){
                this.destroyToken()

                return null
            }else
                return token
        },

        destroyToken () {
            store.dispatch('authenticatedUser',null)
            store.dispatch('isAuthenticated',false)
            store.dispatch('isAdmin',false)
            localStorage.removeItem('token')
            localStorage.removeItem('expiration')
            localStorage.removeItem('user')
        },

        isAuthenticated () {
            if(this.getToken())
                return true
            else
                return false
        },

        setAuthenticatedUser (data) {
            localStorage.setItem('user',JSON.stringify(data))
        },
        getAuthenticatedUser () {
            var user = JSON.parse(localStorage.getItem('user'))

            return user
        }
    }

    Object.defineProperties(Vue.prototype,{
        $auth: {
            get () {
                return Vue.auth
            }
        }
    })
}