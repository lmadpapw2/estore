/**
 * Created by thr0m on 26/03/2017.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {
            path: '/',
            redirect: '/landing'
        },
        {
            path: '/landing',
            component: require('./components/landing/landing.vue'),
            name: 'landing'
        },
        {
            path: '/login',
            component: require('./components/authentication/login.vue'),
            name: 'login',
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/register',
            component: require('./components/authentication/register.vue'),
            name: 'register',
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/product/:id(\\d+)',
            name: 'view.product',
            component: require('./components/product/view.vue'),
            meta: {
                forAuth: true
            },
        },
        {
            path: '/search',
            name: 'view.product.search',
            component: require('./components/product/find.vue'),
            meta: {
                forAuth: true
            },
        },
        {
            path: '/cat',
            component: require('./components/catalog/catalog.vue'),
            meta: {
                forAuth: true
            },
            children: [
                {
                    path: '',
                },
                {
                    path: ':id(\\d+)',
                    component: require('./components/catalog/products.vue'),
                    name: 'view.category',
                    meta: {
                        forAuth: true
                    },
                }
            ]
        },
        {
            path: '/user/:id(\\d+)',
            component: require('./components/user/Profile.vue'),
            name: 'user',
            meta: {
                forAuth: true
            },
            children: [
                {
                    path: 'productos',
                    component: require('./components/user/product/list.vue'),
                    name: 'user.products'
                },
                {
                    path: 'productos/view/:prod(\\d+)',
                    component: require('./components/user/product/view.vue'),
                    name: 'user.products.view'
                },
                {
                    path: 'productos/new',
                    component: require('./components/user/product/new.vue'),
                    name: 'user.products.new'
                },
                {
                    path: 'update',
                    component: require('./components/user/update.vue'),
                    name: 'user.update'
                }
            ]
        },
        {
            path: '/admin',
            component: require('./components/admin/admin.vue'),
            meta: {
                forAdmin: true
            },
            children: [
                {
                    path: '',
                    redirect: 'dashboard'
                },
                {
                    path: 'dashboard',
                    component: require('./components/admin/dashboard/dashboard.vue')
                },
                {
                    path: 'users',
                    component: require('./components/admin/users/users.vue'),
                    children: [

                    ]
                }
            ]
        }
    ],

    linkActiveClass: 'active',
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    },
    mode: 'history',
})

export default router