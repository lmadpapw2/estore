import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        authenticated: false,
        authUser: {},
        isAdmin: false
    },
    getters: {
        isAuthenticated: state => {
            return state.authenticated
        },
        authenticatedUser: state => {
            return state.authUser
        },
        isAdmin: state => {
            return state.isAdmin
        }
    },
    mutations: {
        isAuthenticated: (state,payload) => {
            state.authenticated = payload
        },
        authenticatedUser: (state,payload) => {
            state.authUser = payload
        },
        isAdmin: (state,payload) => {
            state.isAdmin = payload
        }
    },
    actions: {
        isAuthenticated: ({commit}, payload) => {
            commit('isAuthenticated',payload)
        },
        authenticatedUser: ({commit}, payload) => {
            commit('authenticatedUser',payload)
        },
        isAdmin: ({commit},payload) => {
            commit('isAdmin',payload)
        }
    }
})