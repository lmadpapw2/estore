<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>PceX</title>

        <!-- Latest compiled and minified CSS -->
        {!! Html::style('vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css') !!}
        {!! Html::style('vendor/bootstrap-3.3.7-dist/css/bootstrap.theme.min.css') !!}
        {!! Html::style('vendor/font-awesome-4.7.0/css/font-awesome.min.css') !!}
        {!! Html::style('vendor/animate.css/animate.min.css') !!}
        {!! Html::style('css/app.css') !!}

    </head>
    <body>
        <div id="app">
        </div>

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
        <script src="/js/app.js"></script>
    </body>
</html>
