<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'birthday' => \Carbon\Carbon::now()->subYear(random_int(5,15))->addDay(random_int(1,30))->addMonth(random_int(1,12)),
        'avatar' => $faker->imageUrl(640,480,'people',true)
    ];
});


$factory->define(App\Category::class,function(Faker\Generator $faker) {
    return [
        'description' => $faker->word
    ];
});


$factory->define(App\Product::class, function (Faker\Generator $faker) {
    $user = \App\User::all()->random();
    $category = \App\Category::all()->random();

    return [
        'name' => $faker->word,
        'price' => $faker->randomFloat(2,1,1000),
        'description' => $faker->paragraph(random_int(3,10)),
        'user_id' => $user->id,
        'category_id' => $category->id,
        'stock' => random_int(0,50)
    ];
});


$factory->define(App\product_comment::class,function (Faker\Generator $faker) {
    $user = \App\User::all()->random();
    $product = \App\Product::all()->random();


    return [
        'product_id' => $product->id,
        'user_id' => $user->id,
        'comment' => $faker->paragraph(random_int(1,10)),
        'rating' => random_int(0,5),
    ];
});

$factory->define(App\product_media::class,function(Faker\Generator $faker) {
   return [
       'path' => $faker->imageUrl(640,480,'technics',true)
   ];
});

$factory->define(App\wishlist::class,function(Faker\Generator $faker) {
    $user = \App\User::all()->random();
    $product = \App\Product::all()->random();
    return [
        'product_id' => $product->id,
        'user_id' => $user->id
    ];
});