<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class,50)->create();

        factory(\App\User::class,1)->create([
            'email' => 'fausto.islas@crystalsoft.com.mx',
            'passowrd' => bcrypt('alexze90'),
            'admin' => 1
        ]);

        factory(\App\Category::class,10)->create();

        factory(\App\Product::class,100)->create()->each(function ($p){
            /*factory(\App\product_comment::class,200)->create([
                'product_id' => $p->id,
                'user_id' => $p->user_id
            ]);*/

            factory(\App\product_media::class,random_int(1,5))->create([
                'product_id' => $p->id,
            ]);

/*            factory(\App\wishlist::class,1000)->create([
                'product_id' => $p->id,
                'user_id' => $p->user_id
            ]);*/
        });

        factory(\App\product_comment::class,1000)->create();

        factory(\App\wishlist::class,500)->create();



//         $this->call(UserSeeder::class);
    }
}
