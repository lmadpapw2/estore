<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('users','UserController@store')->name('users.store');

Route::get('landing','ProductController@landing')->name('products.landing');

Route::group(['middleware' => 'auth:api'],function (){

    Route::resource('products','ProductController');

    Route::post('comment/{id}','ProductController@addComment');

    Route::resource('categories','CategoryController');

    Route::resource('users','UserController', ['except' => ['store']]);

    Route::post('avatar/user/{id}','UserController@updateAvatar');

    Route::post('search/products', 'ProductController@search');



    Route::get('/user',function (Request $request){
        return $request->user();
    });

    Route::get('/user/products/{id}', 'UserController@getProducts');

    Route::get('/isAdmin', 'UserController@isAdmin');
});