<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //
    use SoftDeletes;

    public $timestamps = true;

    protected $fillable = [
        'description'
    ];

    protected $dates = [
        'created_at', 'deleted_at', 'updated_at'
    ];

    public function products(){
        return $this->hasMany('App\Product');
    }
}
