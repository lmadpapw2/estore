<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class product_comment extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $fillable = [
        'product_id','user_id','comment','rating','comment_id'
    ];


    public function user(){
        return $this->belongsTo('App\User');
    }

    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function comment(){
        return $this->belongsTo('App\product_comment','comment_id');
    }

    public function responses(){
        return $this->hasMany('App\product_comment','comment_id');
    }
}
