<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use SoftDeletes,HasApiTokens,Notifiable;

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','birthday', 'avatar', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','admin'
    ];

    protected $casts = [
        'admin' => 'boolean'
    ];


    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function comments()
    {
        return $this->hasMany('App\product_comment');
    }

    public function wishlist()
    {
        return $this->belongsToMany('App\Product','wishlists');
    }
}
