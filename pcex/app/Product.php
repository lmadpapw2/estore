<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $fillable = [
        'name', 'description', 'price', 'user_id', 'category_id','stock'
    ];

    protected $dates = [
        'created_at', 'deleted_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function media()
    {
        return $this->hasMany('App\product_media');
    }

    public function comments()
    {
        return $this->hasMany('App\product_comment');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }
}
