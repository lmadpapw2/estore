<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Mockery\Exception;

class UserController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth:api')->except('store');
//        $this->middleware('auth')->only('store');
    }

    public function index()
    {
        return User::all();
    }

    public function show($id)
    {
        return User::find($id);
    }

    public function store(Request $request)
    {
        try {

            $users = User::where('email','=',$request->email)->get()->count();

            if($users > 0){
                return response()->json(['result' => false, 'msg' => 'Ya hay una cuenta con ese correo.']);
            }else{
                $user = new User();
                $user->fill($request->all());
                $user->password = bcrypt($user->password);
                $user->avatar = "img/default_avatar.png";
                $user->save();

                return response()->json(['result' => true]);
            }

        } catch (QueryException $exception) {
            return response()->json(['result' => false, 'msg' => 'Ocurrio algo al registrarse, favor de contactar al Administrador.']);
        }
    }


    public function update(Request $request,$id){
        try {

            $users = User::where('email','=',$request->email,'id','!=',$id)->get()->count();
            if($users > 0){
                return response()->json(['result' => false, 'msg' => 'Ya hay una cuenta con ese correo.']);
            }else{
                $user = User::find($id);
                $user->fill($request->all());

                if(isset($request->password)){
                    $user->password = bcrypt($user->password);
                }

                $user->save();

                return response()->json(['result' => true, 'msg' => 'Información actualizada correctamente.']);
            }
        }catch (QueryException $exception) {
            return response()->json(['result' => false, 'msg' => 'Ocurrio algo actualizar, favor de contactar al Administrador.']);
        }
    }


    public function isAdmin(Request $request){


        $result = $request->user()->admin;

        return response()->json(['isAdmin' => $result]);
    }


    public function getProducts($id){
        $productos = User::with('products.media')->find($id);


        return response()->json($productos->products);
    }


    public function updateAvatar(Request $request, $id){
        try{
            $user = User::find($id);

            $img = $request->file('avatar');

            if ($img){
                $pattern = "img/users/".$user->id."*";
                array_map("unlink",glob($pattern));

                $imgname = $user->id.".".$img->extension();
                $uFile = $img->move('img/users',$imgname);

                if($uFile){
                    $user->avatar = asset("img/users/".$imgname);
                    $user->save();
                }
            }

            return response()->json(['msg' => 'Avatar Actualizado.','url' => $user->avatar]);
        }catch (Exception $e){
            return response()->json(['msg' => 'Ocurrio un problema.'],418);
        }
    }
}
