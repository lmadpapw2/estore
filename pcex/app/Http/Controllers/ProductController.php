<?php

namespace App\Http\Controllers;

use App\Product;
use App\product_comment;
use App\product_media;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class ProductController extends Controller
{
    public function index()
    {
        return Product::all();
    }

    public function show($id){
        return Product::with('comments.user','comments.responses.user','media','user')->find($id);
    }

    public function landing()
    {
        $productos = Product::with('media','comments','user')->orderBy('created_at', 'desc')->take(18)->get();
        return $productos;
    }

    public function update(Request $request,$id){
        try{
            $producto = Product::find($id);

            $producto->category_id = $request->category_id;
            $producto->name = $request->name;
            $producto->description = $request->description;
            $producto->price = $request->price;
            $producto->stock = $request->stock;

            $producto->save();

            return response()->json(['msg' => 'Producto Actualizado Correctamente.']);

        }catch (Exception $exception){
            return response()->json(['msg' => 'Ocurrio un problema.'],418);
        }

    }


    public function destroy($id){
        try{
            $producto = Product::find($id);

            $producto->delete();

            return response()->json(['msg' => 'Producto Eliminado correctamente.']);

        }catch (Exception $exception){
            return response()->json(['msg' => 'Ocurrio un problema.'],418);
        }

    }

    public function store(Request $request){
        try{

            $producto = new Product();

            $producto->name = $request->nombre;
            $producto->price = $request->price;
            $producto->user_id = Auth::user()->id;
            $producto->category_id = $request->category;
            $producto->stock = $request->stock;
            $producto->description = $request->description;

            $producto->save();

            foreach ($request->file('media') as $img){
                $imgname = Carbon::now()->timestamp . $img->getClientOriginalName();

                $uFile = $img->move('img/products',$imgname);

                if($uFile){
                    $media = new product_media();
                    $media->product_id = $producto->id;
                    $media->path = asset('img/products/'.$imgname);
                    $media->save();
                }
            }

            return response()->json(['msg' => 'Producto agregado correctamente.']);
        }catch (Exception $ex){
            return response()->json(['msg' => 'Ocurrio un problema.'],418);
        }
    }


    public function addComment(Request $request,$id){
        try{

            $comment = new product_comment();
            $comment->product_id = $id;
            $comment->user_id = Auth::user()->id;
            $comment->comment = $request->comment;

            $comment->save();

            return response()->json(['msg' => 'Comentario agregado correctamente.']);
        }catch (Exception $ex){
            return response()->json(['msg' => 'Ocurrio un problema.'],418);
        }
    }

    public function search(Request $request){
        $products = Product::with('media','user')->where('description','LIKE','%'.$request->find.'%')->orWhere('name','LIKE','%'.$request->find.'%')->get();

        return $products;
    }
}
