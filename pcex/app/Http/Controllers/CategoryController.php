<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //

    public function index(){
        return Category::all();
    }

    public function show($id){
        $cats = Category::with('products.media','products.comments')->find($id);

        return $cats;
    }

}
